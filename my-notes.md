# Instruction for getting postgres running

# Droplet set up (Digital Ocean)
Mainly chose default settings. Can't remember what I did, but plan on updating this section next time I go through the process.
- Use ubuntu

# Installing dependencies
```bash
# Clone repo
git clone git@gitlab.com:billy-horn/playground.git

# Install dependencies
sudo apt update
apt install docker.io
sudo apt install python3-pip
pip install pgcli

# If pip does not work for pgcli, try:
apt install pgcli

# Install other usefool tools (not required)
apt install net-tools

# Expose ports
sudo ufw allow 5432

# Reboot
sudo reboot

# Run postgres
docker run -it \    
    -e POSTGRES_USER="root" \ 
    -e POSTGRES_PASSWORD="root" \
    -e POSTGRES_DB="ny_taxi" \
    -v data:/var/lib/postgresql/data \    -p 5432:5432 \    
    postgres:13

# Once docker container is running , bash into database from server terminal:
pgcli -h localhost -p 5432 -u root -d ny_taxi
```

# Download DBeaver Community edition (free and open-source!)

This is not required, but a UI tool to investigate/view the PG database is super helpful.

After downloading DBeaver, add a postgres connection. I had trouble connecting, but in my case it was two fold:

Ensure your port for Postgres is exposed on the droplet (5432). You can if this is exposed by running this on your server terminal:
```bash
netstat -ant
```
You should see something like:
```bash
tcp6       0      0 :::5432                 :::*                    LISTEN
```

Next, try pinging that port from your local terminal:
```bash
nc -vz <ip_address_of_droplet> <port_to_ping>
```